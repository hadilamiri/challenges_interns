package com.challenge.remuneration.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Survey {
    @JsonProperty("Timestamp")
    public String timestamp;
    @JsonProperty("Employer")
    public String employer;
    @JsonProperty("Location")
    public String location;
    @JsonProperty("Job Title")
    public String jobTitle;
    @JsonProperty("Years at Employer")
    public String yearsAtEmployer;
    @JsonProperty("Years of Experience")
    public String yearsOfExperience;
    @JsonProperty("Annual Base Pay")
    public String annualBasePay;
    @JsonProperty("Signing Bonus")
    public String signingBonus;
    @JsonProperty("Annual Bonus")
    public String annualBonus;
    @JsonProperty("Annual Stock Value/Bonus")
    public String annualStockValueBonus;
    @JsonProperty("Gender")
    public String gender;
    @JsonProperty("Additional Comments")
    public String additionalComments;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getYearsAtEmployer() {
        return yearsAtEmployer;
    }

    public void setYearsAtEmployer(String yearsAtEmployer) {
        this.yearsAtEmployer = yearsAtEmployer;
    }

    public String getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(String yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getAnnualBasePay() {
        return annualBasePay;
    }

    public void setAnnualBasePay(String annualBasePay) {
        this.annualBasePay = annualBasePay;
    }

    public String getSigningBonus() {
        return signingBonus;
    }

    public void setSigningBonus(String signingBonus) {
        this.signingBonus = signingBonus;
    }

    public String getAnnualBonus() {
        return annualBonus;
    }

    public void setAnnualBonus(String annualBonus) {
        this.annualBonus = annualBonus;
    }

    public String getAnnualStockValueBonus() {
        return annualStockValueBonus;
    }

    public void setAnnualStockValueBonus(String annualStockValueBonus) {
        this.annualStockValueBonus = annualStockValueBonus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAdditionalComments() {
        return additionalComments;
    }

    public void setAdditionalComments(String additionalComments) {
        this.additionalComments = additionalComments;
    }

    @Override
    public String toString() {
        return "SurveyOne{" +
                "timestamp='" + timestamp + '\'' +
                ", employer='" + employer + '\'' +
                ", location='" + location + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", yearsAtEmployer='" + yearsAtEmployer + '\'' +
                ", yearsOfExperience='" + yearsOfExperience + '\'' +
                ", annualBasePay='" + annualBasePay + '\'' +
                ", signingBonus='" + signingBonus + '\'' +
                ", annualBonus='" + annualBonus + '\'' +
                ", annualStockValueBonus='" + annualStockValueBonus + '\'' +
                ", gender='" + gender + '\'' +
                ", additionalComments='" + additionalComments + '\'' +
                '}';
    }
}

