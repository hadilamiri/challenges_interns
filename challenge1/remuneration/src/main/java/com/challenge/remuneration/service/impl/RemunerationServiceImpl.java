package com.challenge.remuneration.service.impl;

import com.challenge.remuneration.service.RemunerationService;
import com.challenge.remuneration.utils.FileUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RemunerationServiceImpl implements RemunerationService {

    public ArrayList<Map<String, String>> getRemunerationDataBy(Map<String,String> allRequestParams) throws IOException {
        ArrayList<Map<String,String>> result =
                new ObjectMapper().readValue(Files.readAllBytes(FileUtils.readFile()), ArrayList.class);
        ArrayList<Map<String,String>> cpy = new ArrayList<Map<String,String>>();
        for (String key : allRequestParams.keySet()) {
            if (key.equals("fields")) {
                String[] fields = allRequestParams.get("fields").split(",");
                    for (Map<String,String> k : result){
                        HashMap<String,String> tmp = new HashMap<String,String>();
                       Set<String> stringSet = k.keySet();
                        for (String ss : stringSet) {
                            for (String field : fields) {
                                if (ss.equals(field)) {
                                    tmp.put(ss,k.get(ss));
                                }
                            }
                        }
                        cpy.add(tmp);
                    };
                result = cpy;
            } else if (key.equals("sort")) {
                result.sort(Comparator.comparing(
                        m -> m.get(allRequestParams.get("sort")),
                        Comparator.nullsLast(Comparator.naturalOrder()))
                );
            } else {
                String value = allRequestParams.get(key);
                result = result.stream().filter(map -> map.containsKey(key) && map.get(key).equals(value))
                        .collect(Collectors.toCollection(ArrayList::new));

            }
        }
        return  result;
    }
}
