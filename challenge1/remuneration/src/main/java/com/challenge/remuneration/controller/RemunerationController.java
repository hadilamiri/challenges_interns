package com.challenge.remuneration.controller;

import com.challenge.remuneration.service.RemunerationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

@RestController
public class RemunerationController {

    private final RemunerationService remunerationService;

    public RemunerationController(RemunerationService remunerationService) {
        this.remunerationService = remunerationService;
    }

    @GetMapping("/data_remuneration")
    public ArrayList<Map<String, String>> getRemuneration(@RequestParam Map<String,String> allRequestParams) throws IOException {

        return remunerationService.getRemunerationDataBy(allRequestParams);

    }
}
